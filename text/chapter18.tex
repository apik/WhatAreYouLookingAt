%----------------------------------------------------------------------------------------
%	CHAPTER 18
%----------------------------------------------------------------------------------------

\chapterimage{art/chapter18/cover.jpg} % Chapter heading image
\chapter{Минимализм: без названия, 1960-1975}

Все мы видели их, находились рядом с ними – этими спокойными и
задумчивыми молчунами. Они обладают мощной внутренней силой, им
никогда не нужно прогибаться, идти на компромисс; они окружены особой
аурой, требующей уважения. Они не вульгарны, в них есть свой шик и
стиль, они держатся с достоинством, заставляя всех вокруг чувствовать
себя идиотами. Да, еще в них есть некоторая холодность, загадочность. 

О чем это я? Конечно, о минималистских скульптурах. Об этих
лаконичных, строгих трехмерных кубах и прямоугольниках, сделанных из
промышленных материалов; немые стражи, они занимают выгодные позиции и
доминируют в пространстве галереи. Они – продукт 1960-х годов наряду
со студенческими протестами и свободной любовью. Разве что эти
молчащие произведения искусства рассчитаны не на эмоции, а скорее на
вдумчивое созерцание. В них есть некая сдержанная прозаичность –
антитеза пафосной болтовне окружающего мира. 

Минимализм появился под влиянием самых разных факторов – начиная от
Пенсильванской железной дороги и заканчивая сюрреализмом Андре
Бретона. В нем очень много модернистской эстетики Баухауса, щедро
приправленной русским конструктивизмом. И, при всей своей очевидной
пассивности, эти скульптуры на самом деле тесно связаны с искусством
перформанса, где зритель выступает в качестве исполнителя. Потому что
в сознании скульпторов-минималистов их работы по-настоящему «оживают»
только среди людей. Тогда они выполняют задачу, ради которой и были
созданы: повлиять на то место, где они установлены, и, самое главное,
на тех, кто в нем находится. Речь не о том, чтобы мы просто
полюбовались их угловатой элегантностью; мы должны осознать, как их
присутствие изменило нас и окружающее пространство. 

Я с опаской использую слово «скульптура» для описания трехмерных
минималистских работ, поскольку художники-минималисты давно отказались
от этого термина, связанного с искусством иллюзии, иначе говоря, с
традиционной скульптурой из материала, который скульптор обработал,
чтобы придать ему сходство с каким-то существом или предметом (скажем,
кусок мрамора, преобразованный в человеческую фигуру). Для
минималистов это было неприемлемо, они все понимали буквально. Если уж
делали объект из дерева, стали или пластика, так и получался объект из
дерева, стали или пластика, и не более того. Для описания своих
произведений они придумали альтернативный словарь. Одним из первых в
нем появилось словечко «объект». Не бессмысленно, хотя и не слишком
образно; в конце концов, скульптура действительно является объектом.
Следующие два определения страдали той же проблемой чрезмерной
буквальности: «трехмерная работа» и «структура». Наконец (не иначе как
от отчаяния) было введено слово «предложение». Мало того что несколько
унизительное, да и вообще двусмысленное, оно представлялось совершенно
неуместным, когда речь заходила о металлическом кубе, занимающем
площадь в два с половиной квадратных метра. Это, я бы сказал, не
предложение – это заявление. Так что из чисто рациональных соображений
я пойду наперекор воле скульпторов-минималистов и все же назову их
объемные творения все тем же словом: скульптуры. 

Искусство минималистов в каком-то смысле ничем не отличается от
искусства любого другого времени. В конце концов, искусство всегда
стремилось создать из хаоса некоторый порядок. Принципами упорядочения
могут быть решетки «Де Стиль» или пересекающиеся плоскости кубизма.
Даже анархический нигилизм дада ставил своей задачей избавить мир от
разложения и упадка и установить новый мировой порядок. В общем, цель
всегда была одна: поставить жизнь под контроль. Просто минималисты
работали над этим чуть активнее, чем все их предшественники. В списке
«активистов» – сплошь американцы, все мужчины, все белые, и это тоже в
какой-то степени отражение природы минимализма. 

Итак, перед нами еще один джентльменский клуб современного искусства.
Посмотрите на работы его участников – все, как одна, с мужским
характером. Художники этого направления тяготели к серьезному
искусству, с холодным механистическим качеством и маниакальным
вниманием к деталям. Рука художника и его присутствие здесь почти
незаметны – произведение искусства больше напоминает продукт
промышленной сборки. В минималистских скульптурах нет той романтики,
которая передается физическими усилиями ваятеля, работающего,
например, с камнем. У минималистов процесс обходится без кровавых
мозолей и взмокшего от пота лба. Возможно, все это присуще труду
сварщика, плотника или монтажника – но только не художника. 

Минималисты больше напоминают архитекторов, которые составляют планы,
отдают распоряжения и контролируют процесс производства. В этом нет
ничего плохого; всем известно, что великий фламандец Питер Пауль
Рубенс привлекал помощников, чтобы те писали для него картины. Но он
это делал лишь с целью повысить производительность труда (иными
словами, авторство идеи «художник-бизнесмен» принадлежит вовсе не
Уорхолу, Кунсу или Херсту), а заодно учил подмастерий имитировать его
стиль. Минималисты же стремились действовать наоборот. Как и художники
американского поп-арта, они хотели обезличить произведения, удалить из
них все следы своего присутствия и авторскую субъективность. 

Их задачей было заставить зрителя взаимодействовать непосредственно с
физическим объектом, не отвлекаясь на личность творца. Некоторые, как
Дональд Джадд\footnote{\url{https://www.wikiart.org/en/donald-judd}}
(1928–1994), даже перестали давать своим произведениям названия. В
результате мы видим огромное количество работ Джадда, гордо носящих
одно и то же имя – «Без названия», и лишь дата работы помогает сузить
поиск. Может показаться странным, но Джадд, как и другие минималисты,
верил в необходимость удаления всех лишних деталей: «Чем больше в
работе элементов, тем быстрее их упорядоченность становится главным в
произведении и, таким образом, уводит от формы». 

Дональд Джадд начинал как абстрактный экспрессионист – огромными
полотнами с преобладанием кроваво-красного цвета – красного кадмия.
Впоследствии он отказался от абстрактного экспрессионизма и станковой
живописи, но сохранил верность красному кадмию. Отказаться от холста
Джадда заставила его минималистская философия. Проблему живописи Джадд
видел в том, что зритель не воспринимает холст и изображение как
единое целое. Когда мы смотрим на картину – даже если это плоская
монохромная абстракция, – то думаем только об образе и не пытаемся
выяснить, на чем все это нарисовано. Зачем, ведь смысл не в этом? Но,
с другой стороны, утром, надевая рубашку или вытираясь полотенцем, мы
почему-то думаем о материале и о том, что на нем написано или
нарисовано, воспринимая этот объект уже как единое целое. Именно к
такой целостности стремился Джадд в своем творчестве. Он нашел ее в
скульптуре. 


\begin{figure}[h!]
\centering\includegraphics[width=\textwidth]{art/chapter18/T06524_10.jpg}
\caption{Дональд Джадд. \textit{«Без названия»} (1972)}
\label{fig:noname-1972}
\end{figure}


«Без названия» (1972, ил.~\ref{fig:noname-1972}) представляет собой
открытый полированный медный ящик чуть меньше метра высотой и чуть
более полутора метров шириной. Дно Джадд окрасил своим любимым красным
кадмием. И… э-э… собственно, все. «Без названия» ничего не
символизирует и ни на что не намекает. Это всего-навсего медный ящик с
красным дном. Но как же так – ведь это произведение искусства? В чем
же его смысл? Ответ простой: в том, чтобы просто увидеть и оценить
предмет с точки зрения эстетической и физической: как он выглядит,
какие чувства вызывает. Никто не требует от вас интерпретировать
работу и уж тем более искать в ней скрытый смысл, которого нет. И это,
на мой взгляд, облегчает зрителю задачу. На сей раз от него не ждут
никаких специальных знаний, ему просто нужно принять решение: нравится
или нет. 

Мне – нравится.
\begin{figure}[t]
\centering\includegraphics[height=0.6\textheight]{art/chapter18/Donald-Judd-Untitled-Stacks.jpg}
\caption{Дональд Джадд. \textit{«Без названия (Стопка)»} (1967)}
\label{fig:noname-stack-1967}
\end{figure}

Я нахожу эту простоту завораживающей, текстурированная поверхность
медного ящика кажется мне теплой и звонкой, а элегантные острые углы
рассекают окружающее пространство с точностью лазера. Подойдите на шаг
ближе, и вы увидите, как из чрева ящика поднимается красный
вулканический пар, оттеняя бритвенную остроту краев. Если вы
наклонитесь и заглянете внутрь, то увидите, что эффект дымки,
создаваемый красным кадмием, вызывает воспоминания о позднем летнем
закате. Внутренние стенки из меди как будто тонут в красном вине. Вы
тоже ощутите его пьянящее действие, когда, постояв подольше, различите
два куба, затем три, а потом и целое множество, по мере того как
блестящая медная поверхность будет творить свою оптическую магию. 

В этот момент вам, наверное, захочется отойти и присмотреться к тому,
как это было сделано (рабочими по техническому заданию Джадда). И вы
увидите все мелкие погрешности, неровную текстуру меди, вмятины и
царапины. А еще вы наверняка заметите, что бока не идеально ровные, а
пара болтов вкручена слишком глубоко. Это несовершенства жизни, их не
скроешь, как ни старайся. Сделайте шаг назад и обойдите ящик кругом –
вы будете поражены тем, как медь усиливает ваше внимание к
переменчивому дневному освещению, что в свою очередь повышает общую
восприимчивость к физической среде. А потом, уже по пути в другой зал,
вы обязательно обернетесь и бросите прощальный взгляд на объект –
гарантирую. И медный ящик с ярко-красным дном врежется вам в память
навсегда, потому что он попросту красив. 

Джадд отказывался от всего, что могло бы помешать раскрыть природу
материала или отвлечь зрителя от чистого созерцания. Медная коробка,
как, собственно, и в той или иной степени все скульптуры Джадда,
позволяет зрителю застыть в настоящем времени. В его работах нет ни
истории, ни аллегории, так что голову ни над чем ломать не нужно.
Ничто не отвлекает. Джадд считал, что идея спонтанности уже полностью
воплощена Поллоком в капельной живописи. Он принимал как должное, что
«мир на девяносто процентов состоит из вероятности и случая». Эта
мысль стала для Джадда отправной точкой. Вот зачем он и упростил свою
работу – чтобы исключить случайность. 




Правда, художник все-таки положился на волю случая, когда затеял серию
«стопок» – таких как «Без названия (Стопка)» (1967,
ил.~\ref{fig:noname-stack-1967}), которая напоминает дюжину торчащих
из стены, одна над другой, полок. Сделать фрагментированную
скульптуру, когда твоя основная цель – представить единый и несложный
объект, – затея рискованная. Но Джадд в себе уверен. Каждая полка, или
шаг, произведена из оцинкованной стали и покрашена зеленой краской. 12
единиц, изготовленных в металлическом цехе в Нью-Джерси, абсолютно
идентичны. Пожалуй, Джадд при всем желании не смог бы сделать больше,
чтобы дистанцироваться от живописной техники абстрактных
экспрессионистов. 

Он протестовал против романтического мифа о том, что взмахом кисти
художник может передать какую-то мистическую внутреннюю правду. Джадд
был рационалистом до мозга костей. С помощью монотонного повторения он
развенчивает идею, будто каждое живописное движение несет свой особый
смысл и заслуживает внимания. По его мнению, смысл можно найти только
в абстрактном единстве. Скульптурой «Без названия (Стопка)» Джадд
добился того, что 12 отдельных элементов выступают как единое целое.
На самом деле в этой работе 23 отдельных элемента. Это 12 полок и 11
пространств между ними. Каждый «шаг» глубиной 22,8 сантиметра,
расстояние между полками тоже 22,8 сантиметра. В отличие от
традиционной скульптуры в этой работе нет никакой иерархии; нижняя
ступенька (цоколь) имеет такую же ценность, как верхняя (венец
творения). Всю конструкцию удерживает вместе – и в этом гений Джадда –
нечто невидимое. Автор назвал это поляризацией; мне больше по душе
слово «напряжение». 

Похожую идею можно разглядеть в картинах Фрэнка Стеллы
% \footnote{\url{https://www.wikiart.org/en/frank-stella}}
(род. 1936). Он был почти на десять лет моложе Дональда Джадда, но
прошел почти такой же путь, начав с абстрактного экспрессионизма и
разделяя недовольство Джадда ограниченными возможностями искусства
своего времени. Когда ему было двадцать три, Стелла остановился на
упрощенной версии абстрактного экспрессионизма и уже в ней состоялся
как крупный художник. Дороти Каннинг, всеми уважаемый куратор МоМА,
заметила «Черные картины» молодого Фрэнка в нью-йоркской студии в 1959
году и была поражена их простотой и оригинальностью.

\begin{figure}[h!]
\centering\includegraphics[width=\textwidth]{art/chapter18/the-marriage-of-reason-and-squalor-1959.jpg}
\caption{Фрэнк Стелла. \textit{«Союз разума и нищеты, II»} (1959)}
\label{fig:union-mind}
\end{figure}



Одна из этих картин, «Союз разума и нищеты, II» (1959,
ил.~\ref{fig:union-mind}), содержит две идентичные черно-белые
фигуры, изображенные бок о бок. В середине каждой фигуры оставлена
тонкая полоска незакрашенного холста, опускающаяся на две трети высоты
полотна. Полоска обведена толстой черной линией в форме буквы П. И
далее мотив повторяется: вокруг П остается тонкая линия холста, в свою
очередь обрамленная черной каймой. В результате получилась картина,
заставляющая вспомнить костюмную ткань. 


\begin{figure}[h!]
\centering
\includegraphics[height=0.4\textheight]{art/chapter18/combinations-satelite.jpg}
\includegraphics[height=0.4\textheight]{art/chapter18/combinations-magician.jpg}
\caption{Роберт Раушенберг. \textit{«Спутник»} (1955), \textit{«Волшебник»} (1959)}
\label{fig:combinations-2}
\end{figure}


Каннинг решила включить работы Стеллы в экспозицию выставки
американского авангарда. Для этой выставки она отобрала «Комбинации»
Раушенберга (ил.~\ref{fig:combinations-2}),
«Мишени» (ил.~\ref{fig:targets}) и «Флаги» (ил.~\ref{fig:white-flag}) Джаспера
Джонса (источники вдохновения для
Стеллы) и работы ряда других художников. Выставка «Шестнадцать
американцев»\footnote{\url{https://www.moma.org/calendar/exhibitions/2877}}
(МоМА, декабрь 1959 – февраль 1960) должна была заявить о 
новых веяниях в художественном мире, возникших после того, как
современное искусство вырвалась из-под контроля эмоциональной живописи
первых абстрактных экспрессионистов. Критики встретили работы молодых
– и прежде всего картины Стеллы – без особого энтузиазма. Кто-то
назвал его работы «невыразимо скучными». Джадд так не думал. Он точно
знал, чего добивался Стелла, потому что сам пытался достичь того же,
только в скульптуре. Это была буквальность, прямота; или, как ответил
Стелла на просьбу объяснить его картины: «То, что вы видите, – это то,
что вы видите». Джадд пришел к исключению всех лишних элементов ради
упрощения. Стелла выбрал симметрию как бесконечный повтор одного и
того же изображения. Это был минималистский подход. 



\begin{figure}[h!]
\centering
\includegraphics[height=0.3\textheight]{art/chapter18/target-4-faces.jpg}
\hspace{1cm}
\includegraphics[height=0.3\textheight]{art/chapter18/green-target.jpg}
\caption{Джаспер Джонс. \textit{«Мишень с четырьмя лицами»} (1955), \textit{«Зеленая мишень»} (1955)}
\label{fig:targets}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=0.8\textwidth]{art/chapter18/white-flag.jpg}
\caption{Джаспер Джонс. \textit{«Белый флаг»} (1955)}
\label{fig:white-flag}
\end{figure}



Стелла, как и Джадд, хотел убрать из своих работ любой намек на
иллюзию. Он делал это и своими черно-белыми картинами, и цветными –
такими как «Hyena Stomp» (1962, ил.~\ref{fig:hyena}). Здесь Фрэнк
использовал 11 цветов (оттенки желтого, красного, зеленого и синего),
из которых сложил узор наподобие лабиринта, по спирали закручивая его
из центра в конечную точку в правом верхнем углу, что слегка нарушает
симметрию композиции. Стелла играет с идеей синкопы, неожиданно меняя
ритм. Картина названа по композиции джазового музыканта Джелли Ролла
Мортона. Это полотно также демонстрирует, насколько далеко Стелла ушел
от первоисточников абстрактного экспрессионизма. Хотя его картины в
высшей степени абстрактны и экспрессивны, очевидно, что они заранее
продуманы. Поллок в своих спонтанных порывах был верен автоматизму
сюрреалистов. Стелла в этом смысле ближе к концептуализму – он ничего
не делал просто так. Для Фрэнка главной была мысль; живописью, как он
считал, может заниматься любой человек. 

\begin{figure}[h!]
\centering\includegraphics[width=\textwidth]{art/chapter18/T00730_10.jpg}
\caption{Фрэнк Стелла. \textit{«Hyena Stomp»} (1962)}
\label{fig:hyena}
\end{figure}


Влияние Фрэнка Стеллы на минимализм трудно переоценить. Он дал
интеллектуальный стимул Джадду а замечание, которое он сделал Карлу
Андре\footnote{\url{https://www.wikiart.org/en/carl-andre}}
(род. 1935), в корне изменило жизнь скульптора. 

Это произошло, когда Стелла и Андре вместе снимали студию в Нью-Йорке
в конце 1950-х – начале 1960-х годов. Андре был в восторге от
симметричных концентрических картин Стеллы, с другой стороны, он
оставался преданным поклонником Константина Бранкузи и работал над
деревянным тотемным столбом в примитивном стиле румынского скульптора.
Андре деловито вырезал по дереву какие-то современные геометрические
фигуры, когда подошел Стелла и сказал, что получается «чертовски
хорошо». Потом обошел столб, остановился с обратной стороны, которой
еще не коснулась рука Андре, и проронил: «Знаешь, а это тоже хорошая
скульптура». Кто-то, наверное, и рассердился бы, услышав после долгих
часов кропотливой работы над одним фрагментом, что нетронутая часть не
хуже. Но только не Андре – он согласился, ответив: «Да, она
действительно намного лучше, чем выпиленная». Поразмыслив, Карл понял,
что его усилия умалили значимость объекта как произведения искусства.
Позже он вспоминал то мгновение: «И тогда я решил, что больше не буду
выпиливать деревяшки… я буду представлять их в пространстве». 

\begin{figure}[t]
\centering\includegraphics[width=\textwidth]{art/introduction/equivalent8.jpg}
\caption{Карл Андрэ. \textit{«Эквивалент VIII»}, 1966}
\label{fig:eqivalen8-ch18}
\end{figure}

Что он и сделал. Для Андре скульптуры были больше похожи на то, что мы
сегодня называем инсталляцией; то есть произведениями искусства,
призванными физически реагировать и влиять на пространство, в котором
они установлены, и на тех, кто в этом пространстве присутствует. Когда
это возможно, Карл предпочитает участвовать в установке своих
скульптур для экспозиции в галерее. Точное расположение скульптуры и
то, как она включена в пространство, чрезвычайно важно для Андре, ведь
от этого зависит вся композиция. Потому удивительно, что его
скульптуры, созданные в расчете на взаимодействие с окружением, так
легко пропустить, когда смотришь экспозицию. Как и другие
скульпторы-минималисты, Андре отказался от постамента – он ставил свои
произведения прямо на пол. Это, по его замыслу, добавляет «прямоты» и
дистанцирует его работы от фигуративной скульптуры прошлого. В
принципе нормально, но для автора, создающего скульптуру из небольших
и плоских кусков промышленных материалов, это создает некоторые
проблемы. Его скульптуры поневоле становятся частью пола; это,
конечно, новаторский подход, но в результате зритель может их попросту
не заметить. Я сам не раз был свидетелем того, как посетители спокойно
перешагивали через напольную скульптуру – как через коврик у двери. 


Пожалуй, наибольшую известность принесла Карлу Андре скульптура
«Эквивалент VIII» (1966, ил.~\ref{fig:eqivalen8-ch18})– 120
огнеупорных кирпичей, уложенных в два слоя в форме прямоугольника; это
за нее Тейт досталось от СМИ в 1976 году. Скульптура как нельзя лучше
отражает минималистский стиль Андре, поскольку она сделана из
промышленного материала, в ней отсутствует иерархия элементов, и в ее
основе лежит имперская система мер. Она в высшей степени абстрактна,
лаконична, симметрична, тщательно выстроена и лишена даже намека на
художественные излишества. Скульптор сделал «Эквивалент VIII»
максимально обезличенным, чтобы у зрителя не было ни малейшего
интереса к автору – как и возможности «прочитать» работу. Он не хочет,
чтобы мы думали, будто «Эквивалент VIII» может быть чем-то другим,
кроме того, чем он является: прямоугольником, выложенным из 120
огнеупоров. 


В отличие от других минималистов, да и вообще от почти любого мастера
творящего, Андре не использовал болты, клей, краски, чтобы связать
воедино отдельные элементы своей композиции, они остаются свободными.
Но в то же время они сцеплены; не физически, а в том же смысле, что и
«стопки» Джадда. Оба стремились к тому, чтобы их работу воспринимали
как единое целое. Не скрепляя кирпичи (он попросту клал их рядом или
друг над другом), Андре, по сути, создавал то же напряжение между
ними, как и Джадд, оставивший пространство между окрашенными «шагами»
своих «Стопок». Щели между кирпичами как раз и создают впечатление
прочной связанности. 

Такой подход Андре к целостности был чреват определенными проблемами.
Известно немало случаев, когда посетители утаскивали «кусочек Андре»,
припрятывая кирпичи и другие стройматериалы из его скульптур под
свитерами и куртками. Для них забава, для Андре (как и для музейщиков)
– головная боль. Особенно в 1960-х, когда он только начинал и с
деньгами было туго, а материалы стоили дорого. Безденежье вынудило
Карла Андре пополнять свой скромный бюджет, подрабатывая кондуктором
на Пенсильванской железной дороге. Эта работа в конечном счете помогла
ему прийти к славе и, если не к богатству, то во всяком случае к
комфортной жизни. 


\begin{figure}[t]
\centering\includegraphics[width=\textwidth]{art/chapter18/T01767_10.jpg}
\caption{Карл Андрэ. \textit{«144 квадрата магния»}, 1969}
\label{fig:144-squares}
\end{figure}

У нас скульптура обычно ассоциируется с вертикалью. Андре видит ее
иначе. Как известно, он был человеком горизонтали: сказались годы
работы на «железке». Наблюдая за дорогой из кабины машиниста, он видел
в ней – в милях ржавых рельсов с равномерными горизонталями деревянных
шпал – потенциал для скульптуры, то, что потом станет основой его
творчества. Взять, к примеру, скульптуру «144 квадрата магния» (1969,
ил.~\ref{fig:144-squares}), которая состоит из 12 рядов по 12
квадратных магниевых пластин (он сделал еще пять версий – с алюминием,
медью, свинцом, сталью и цинком) – всего 144 штуки. Все пластины
одинакового размера – по 12 дюймов (30 см) – выложены в один большой
квадрат размером 3,6 метра. Ну вот мы и вернулись на территорию
эквивалентов Карла Андре. За исключением того, что на этот раз он
обращается к посетителю с приглашением пройтись по его напольной
скульптуре, «оставив на ней свой след». Впрочем, истинный мотив у
Андре был посложнее. Художник рассчитывал, что опыт хождения по
магниевым пластинам поможет посетителю прочувствовать физические
свойства материала. 


Эта идея восходит к русским конструктивистам, и прежде всего к
Владимиру Татлину, чьим поклонником был Карл Андре. Свой «Угловой
контррельеф» (ил.~\ref{fig:corner-contr}) Татлин тоже очистил от всех
символических значений, чтобы побудить зрителя рассмотреть материалы,
из которых сделан объект, и почувствовать эффект его воздействия на
окружающее пространство. Этот подход к искусству был близок к тому, к
чему пришли американские минималисты через полвека. И за что они были
очень благодарны русскому конструктивисту. Но никто из них не
расстарался так, как Дэн Флавин (1933–1996), посвятивший 39 скульптур
основоположнику конструктивизма. 

\begin{figure}[t]
\centering\includegraphics[width=0.6\textwidth]{art/chapter18/1964-65a.jpg}
\caption{Дэн Флавин. \textit{«Монумент 1 В. Татлину»} (1964)}
\label{fig:tatlin-monument}
\end{figure}

В то время как Татлин конструировал свои работы из материалов,
используемых для строительства современных ему зданий начала XX века
(алюминий, стекло и сталь), Флавин в духе времени предпочел
люминесцентные лампы. У Татлина выбор материалов был выражением его
поддержки революционных идей новой России. У Флавина он продиктован
стремлением объединить историю искусства и современность. Свет всегда
был неотъемлемой частью художественной практики. Собственно, художник
может видеть и творить, только когда есть свет. Караваджо и Рембрандт
писали свои великие картины, используя технику кьяроскуро, подчеркивая
контраст между светом и тенью. Классики – от Тернера до
импрессионистов – постоянно пытались передать мимолетные эффекты
света. Ман Рэй описывал рэйограммы и соляризацию как «живопись
светом». Наконец, не будем забывать про религиозный и духовный
подтекст света, вспомним Библию: «И сказал Бог: да будет свет», «И
назвал Бог свет днем» или «Я свет миру». 

Флавин считал флуоресцентный свет «анонимным и безразличным»: символом
века, который приносит лишь «ограниченный свет». Он и любил его за
безликость, этот продукт массового производства, видел в нем материал,
соединяющий искусство с «повседневной жизнью». Но, как и коллеги по
цеху минималистов, художник настаивал на том, что в его работах нет
никакого скрытого смысла: «Это то, что есть, и ничего больше».
Неоновые трубки были для Флавина способом «игры с пространством»,
возможностью изменить помещение и поведение людей, в нем находящихся,
так что с 1963 года эти трубки становятся для него главным
инструментом творчества. Многочисленные посвящения Татлину, созданные
в течение двух с половиной десятилетий, стали самыми известными
сериями Дэна Флавина; первая скульптура появилась на свет в 1964 году,
а последняя – в 1990-м. Все они имели одно название – «Монумент В.
Татлину» – хотя в дизайне есть кое-какие различия. 


«Монумент 1 В. Татлину» (1964, ил.~\ref{fig:tatlin-monument}), первый
в серии, состоит из семи флуоресцентных трубок, закрепленных на стене,
и самая высокая (2,5 метра) служит центральной осью скульптуры.
Остальные шесть располагаются симметрично, по три с каждой стороны
центральной трубки в порядке убывания по высоте. Скульптура напоминает
очертания манхэттенского небоскреба 1920-х годов или трубы церковного
органа. А вот сходства со знаменитой непостроенной башней Татлина,
«Памятником III Интернационалу» (1920, ил.~\ref{fig:III-intern}), с
которого, видимо, списывалась композиция, добиться не удалось.
Впрочем, Флавина это ничуть не смущало. В непривычно веселой для
серьезного минималиста манере он как-то сказал, что назвал скульптуру
монументом шутки ради, поскольку эта вещь сделана из бросового сырья. 



Несмотря на бедность материала, скульптура с ярко светящимися лампами
производит желаемый эффект. Посетители галереи, заходя в зал, сразу же
устремляются к ней. Это как если бы новелла Кафки «Превращение» стала
реальностью, и люди, обращенные в насекомых, полетели бы на теплый и
яркий свет. Словом, «Монумент» имеет несомненный успех. Флавин, как и
Джадд с его глыбами, добивался того, чтобы неоновые скульптуры
преображали пространство. В этом и состояла миссия минималистов. 

Именно Дэн Флавин помог Солу Левитту
\footnote{\url{https://www.wikiart.org/en/sol-lewitt}} 
(1928–2007) открыть для себя
минималистскую философию, когда в начале 1960-х Левитт работал в
книжном магазине МоМА. Там он и познакомился с несколькими начинающими
художниками, которые подрабатывали в музее, и одним из них был Дэн
Флавин. Флавин рассказывал Солу об искусстве и Нью-Йорке, об
абстрактных экспрессионистах, которые были слишком эгоистичны и всегда
«присутствовали» в своих работах. Он в буквальном смысле вывел Левитта
на свет и показал направление, в котором надо двигаться, чтобы
реализовать свои художественные амбиции. 

Для Левитта идея имела первостепенное значение: произведением
искусства была концепция, а способ ее выражения он рассматривал лишь
как мимолетное наслаждение. Сол не стал бы возражать против того,
чтобы созданный им арт-объект уничтожили, – он вполне мог прожить без
него. Главное, что в столе лежал листок с написанной концепцией. В
таком случае зачем нужен надзор за производством работ, на который
тратили массу времени Джадд, Флавин и Андре? Можно просто отдать
письменные распоряжения – а воплощают идею в жизнь пусть другие. 



Если художники и ремесленники, которых Сол Левитт привлекал для
изготовления своих решеток и белых кубов, сомневались, что делать
дальше ввиду туманности формулировок в инструкции (а эти формулировки
часто включали такие многозначные комментарии, как «не касаясь» или
«прямая линия»), Левитт с удовольствием разрешал интерпретировать свой
замысел так, как исполнители его понимали. В сущности, он даже поощрял
это – видя в таких вмешательствах часть творческого процесса. Сол
настолько ценил художественный вклад помощников, что даже упоминал их
как соавторов своих работ – и таким образом помогал им строить
собственную художественную карьеру. И дело тут не только в великодушии
Левитта – таковы были его взгляды на искусство. 

\begin{figure}[h!]
\centering\includegraphics[width=\textwidth]{art/chapter18/Sol-LeWitt-Serial-Project-I-ABCD-469x210.jpg}
\caption{Сол Левитт. \textit{«Серийный проект, I (ABCD)»} (1966)}
\label{fig:ABCD}
\end{figure}


Несмотря на присущие скульптору тепло и доброту, его работы настолько
строги, что кажутся слишком засушенными. В 1966 году Левитт представил
«Серийный проект I (ABCD)» (ил.~\ref{fig:ABCD}) – скульптуру из
нескольких белых прямоугольных блоков. Одни из них были монолитными, а
другие представляли собой рамочную конструкцию, напоминающую
подмостки. Кубы были разных размеров, но каждый «не выше колена». Все
они стояли на полу, на квадратном сером ковре с нанесенной на него
белой решеткой. Кубы, размещенные в пределах решетки, вместе
напоминали Нью-Йорк с высоты птичьего полета. 


Концепция Левитта заключалась в том, чтобы показать, как некие
предметы могут выглядеть аккуратной организованной группой в одном
контексте и полным хаосом – в другом. Что кажется странным для
композиции, которая напоминает ландшафт из белых кубов. Впрочем,
Левитт сделал важное пояснение, сказав, что целью было «не учить
зрителя, но дать ему информацию», благодаря которой его мысли станут
ясными и четкими, как эти открытые кубы. Манифест Сола Левитта
провозглашал «воссоздание искусства, начиная с одного квадрата», и эту
идею он воплотил в своем «Серийном проекте». 

Левитт, как и обещал, начал работу над арт-объектом с квадрата.
Нарисовал на листке бумаги один куб. Потом еще один, и еще, и еще,
пока лист не был заполнен. Эта конструкция казалась ему организованной
и связной. Но только до тех пор, пока нарисованные кубы не
превратились в трехмерные объекты и не выстроились в виде скульптуры:
тогда он увидел лишь беспорядок – и безмятежность сменилась
беспокойством. Левитт оказался перед лицом непростой визуальной
головоломки, которую сам же и создал. Тогда Сол решил медленно обойти
вокруг скульптуры и посмотреть на нее с разных сторон и ракурсов. По
мере того как его глаза приспосабливались к путанице, впитывая все
больше информации, нагромождение случайных кубов постепенно
выстраивалось в некую привлекательную систему. Левитт обнаружил, что
открытые кубы наполняют комнату светом, и она кажется больше, чем есть
на самом деле. А монолитные кубы выступают в качестве визуальных опор:
возвращают взгляд и мысль к скульптуре. 

«Серийный проект I (ABCD)» – типичное произведение минималистского
искусства, которое с полным правом можно назвать искусством
космического века: математическим, методичным и бесстрастным.
Художники-минималисты исследовали некоторые из тех понятий, над
которыми бились и ученые, разрабатывая ракеты для полета человека на
Луну: материю, системы, объем, последовательности, восприятие и
порядок. И самое главное, как это все относится к нам – обитателям
этого диковинного шарика, называемого планетой Земля. 

За внешним спокойствием минималистских работ скрывалось настойчивое
желание авторов привнести в мир порядок и управляемость. Но не призыв
к насилию и принуждению передали они следующему поколению художников,
детям «детей цветов», в полной мере испытавшим влияние нефтяного
кризиса 1970-х. Им предстояло двигать искусство в совершенно ином
направлении. 

Минимализм подвел черту под модернизмом. Искусство вступало в новую
эпоху – эпоху постмодернизма. 

%%% Local Variables:
%%% mode: plain-tex
%%% TeX-master: "../WhatAreYouLookingAt"
%%% End:
